/*
	Бекэнд-форма добавления комментариев по правкам от клиентов рекламного агенства для менеджеров
*/

import React, { Component } from "react";
import SelectTags from "../select-tags";
import { getTags } from "../../utils";
import exit from "../tag-modal/clear-button.svg";
import error from "../tag-modal/round-error-symbol.svg";
import "./comment-dialog.css";
import TagContainer from '../tag-container'

class ModalWindow extends Component {
  constructor(props) {
    super(props);
    this.uploadRef = React.createRef();

    this.state = {
      name: "",
      description: "",
      priority: "1",
      date: "",
      isDragOver: false,
      savedFile: '',
      uploadFile: '',
      label: 'Карточка клиента',
      selectedTags: [],
      tags: getTags(),
      isChecked: false,
      invalidName: false,
      invalidDate: false,
      add: true
    };
  }

  componentDidMount() {
    const { editionForm } = this.props;
    const { name, description, priority, date, uploadFile, label, selectedTags } = editionForm;
    if (!name) {
      this.setState({
        add: false
      });
      return;
    } else{
      this.setState({
      name: name,
      description: description,
      priority: !priority ? '1' : priority,
      date: date,
      uploadFile: uploadFile,
      label: uploadFile ? "Изменить карточку клиента" : !label ? 'Карточка клиента' : label,
      selectedTags: selectedTags
    });}
    
  }

  onSelectTag = ({tag}) => {
    this.setState(({ selectedTags }) => {
      if (!selectedTags.some((item) => { return item.id === tag.id})) {
        const newSelectedTagsArray = [...selectedTags, tag];
        return {
          selectedTags: newSelectedTagsArray
        };
      }

    });
  };

  getValidAndEdit = event => {
    event.preventDefault();
    if (!/[A-Za-z0-9]/.test(this.state.name))
      this.setState({ invalidName: true });
    else if (this.state.date === undefined)
      this.setState({ invalidDate: true });
    else {
      const state = this.state;
      this.props.onEditItem(event, state);
    }
  };

  getValidAndAdd = event => {
    event.preventDefault();
    if (!/[A-Za-z0-9]/.test(this.state.name))
      this.setState({ invalidName: true });
    else if (this.state.date === undefined)
      this.setState({ invalidDate: true });
    else {
      this.props.onAdd(event, this.state);
    }
  };

  fileSize = (event, droppedFile = null) => {
    let file;

    if(!droppedFile) {
      file = event.target.files[0];
    } else {
      file = droppedFile;
    }
    
    if(file) {
      if (file.size > 204800) {
        alert("Файл слишком большой");
        this.uploadRef.current.value = "";
      } else if(file['type'].split('/')[0] !== 'image') {
        alert("Не изображение");
        this.uploadRef.current.value = "";
      } else {
        var reader = new FileReader();

        reader.onloadend = () => {
          this.setState({
            uploadFile: reader.result,
            savedFile: reader.result,
            label: file.name,
            isDragOver: false
          });
        };
        reader.readAsDataURL(file);
      }
    } else {
      return;
    }
  };

  onDeleteItem = (event, itemId) => {
    event.preventDefault();
    this.setState(({ selectedTags }) => {
      const id = selectedTags.findIndex(el => el.id === itemId);
      const newArr = [
        ...selectedTags.slice(0, id),
        ...selectedTags.slice(id + 1)
      ];
      return {
        selectedTags: newArr
      };
    });
  };

  onDragOver = e => {
    e.preventDefault();
    this.setState({ isDragOver: true });
  };

  onDragLeave = () => {
    this.setState({ isDragOver: false });
  };

  onDrop = (e) => {
    e.preventDefault();
    const file = e.dataTransfer.files[0];
    this.fileSize(e.nativeEvent, file);
  };

  render() {
    const { onExit } = this.props;

    return (
        <div className="d-block modal b-l-light shadow-lg bg-light">
          <form action="submit" className="sub">
            <div className="header-modal h-20 float-right mr-4 mt-3 ">
              <img
                  src={exit}
                  onClick={onExit}
                  className="round"
                  width={35}
                  alt="Закрыть"
              />
            </div>
            <br />
            <div className="main mt-2">
              <input
                  type="text"
                  onChange={event => {
                    this.setState({ name: event.target.value, invalidName: false });
                  }}
                  value={this.state.name}
                  className="m-2 form-control"
                  placeholder="Имя клиента"
                  maxLength={20}
                  autoFocus={true}
                  required={true}
              />
              {this.state.invalidName && (
                  <span className="error">
                <img src={error} width="15" alt="Недопустимое имя" /> Недопустимое имя
              </span>
              )}
              <input
                  type="text"
                  onChange={event => {
                    this.setState({ description: event.target.value });
                  }}
                  value={this.state.description}
                  className="m-2 form-control"
                  placeholder="Описание"
              />
              <span className="ml-2">Приоритет</span>
              <select
                  onChange={event => {
                    this.setState({
                      priority: event.target.value
                    });
                  }}
                  value={this.state.priority}
                  className="m-2"
                  name="Приоритет"
                  id="prior"
                  required={true}
              >
                <option value="1">Низкий</option>
                <option value="2">Средний</option>
                <option value="3">Высокий</option>
              </select>
              <input
                  onChange={event => {
                    this.setState({
                      date: event.target.value,
                      invalidDate: false
                    });
                  }}
                  value={this.state.date}
                  className="m-2 form-control"
                  type="date"
              />

              <div>
                <label
                    htmlFor="uploadFile"
                    className={this.state.isDragOver ? "m-2 foruploadFile foruploadFile--draggingOver" : "m-2 foruploadFile"}
                    onDragOver={this.onDragOver}
                    onDragLeave={this.onDragLeave}
                    onDrop={this.onDrop}
                    style={{
                      backgroundImage: this.state.uploadFile ? `url(${this.state.uploadFile.toString()})` : "none",
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "auto 100%",
                      backgroundPosition: "center"
                    }}
                >
                  {this.state.label}
                </label>
                <input id="uploadFile" className="d-none" type="file" accept="image/*" onChange={this.fileSize} ref={this.uploadRef}/>
                <input type="hidden" name="savedFile" id="savedFile" value={this.state.savedFile} />
              </div>

              <br />
              <SelectTags
                  tags={this.state.tags}
                  onTagSelected={this.onSelectTag}
              />
              {this.state.invalidDate && (
                  <span className="error">
                <img src={error} width="15" alt="Недопустимая дата" /> Недопустимая дата
              </span>
              )}
              <hr />
              <div className="all-my-tags d-flex">
                <TagContainer tags={this.state.selectedTags} onDeleteTag={this.onDeleteItem} />
              </div>
              <div className="content-center">
                {!this.state.add && (
                    <button
                        onClick={this.getValidAndAdd}
                        className="btn btn-cool"
                    >
                      Добавить комментарий
                    </button>
                )}
                {this.state.add && (
                    <button onClick={this.getValidAndEdit} className="btn btn-cool">
                      Изменить комментарий
                    </button>
                )}
              </div>
            </div>
          </form>
        </div>
    );
  }
}
export default ModalWindow;
