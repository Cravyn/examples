/*
	Список комментариев от клиентов рекламного агенства
*/

import React, { Fragment, Component } from "react";
import ItemList from "./component/item-list";
import { getData, resetData, setData, deleteItem } from "./utils";
import ModalWindow from "./component/modal-dialog";
import QueueAnim from "rc-queue-anim";
import SearchBar from "./component/search-bar";
import TagModal from './component/tag-modal';
import { DragDropContext } from "react-beautiful-dnd";
import "./CommentsList.css";

export default class App extends Component {
  state = {
    items: [],
    openModal: false,
    isEditModal: false,
    openModalEdit: false,
    loading: true,
    editBody: {},
    isError: false,
    isErrorDate: false,
    query: "",
    sort:'',
    sortByQuery:'',
    idEdit: 0,
    tagOpen:false,
    searchByTags:false
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState(() => {
        const items = getData();
        return {
          initialItems: items,
          items: items,
          loading: false
        };
      });
    }, 800);
  }

  searchingItem = event => {
    this.setState({
      query: event.target.value
    });
  };

  confirmEdit = (event, body) => {
    this.showModal();
    body.id = this.state.idEdit;
    const idx = this.state.items.findIndex(el => el.id === body.id);
    const newArr = [
      ...this.state.items.slice(0, idx),
      body,
      ...this.state.items.slice(idx + 1)
    ];
    resetData(newArr);
    this.setState({ items: newArr, initialItems: newArr });
  };

  deleteItem = id => {
    this.setState(({ items }) => {
      const newArr = items.filter(item => item.id !== id);

      deleteItem(id);
      return {
        items: newArr,
        initialItems: newArr
      };
    });
  };
  
  tickItem = id => {
    this.setState(({ items }) => {
      const idx = items.findIndex(el => el.id === id);
      const form = items[idx];
      const newItem = { ...form, isChecked: !form.isChecked };

      const newArr = [...items.slice(0, idx), newItem, ...items.slice(idx + 1)];
      resetData(newArr);

      return { items: newArr, initialItems: newArr };
    });
  };
  
  editItem = (body) => {
    this.setState(({ openModal }) => {
      return {
        openModal: !openModal,
        editBody: body,
        idEdit: body.id
      };
    });
  };

  sortItems = () => {
    this.setState(({ sort }) => {
      return{
        sort:!sort
      }
    });
  };
  
  showTag = ()=>{this.setState(({tagOpen})=>{return{tagOpen:!tagOpen}})};
  
  addItem = (event, state) => {
    event.preventDefault();
    const newArr = setData(state);
    this.setState({ items: newArr, initialItems: newArr });
    this.showModal();
  };
  
  showModal = () => {
    this.setState(({ openModal }) => {
      if (openModal) {
        return {
          editBody: {},
          openModal: !openModal
        };
      }
      return {
        openModal: !openModal
      };
    });
  };
  
  onDragEnd = ({source, destination}) => {
    if (!destination || (source.droppableId === destination.droppableId && source.index === destination.index)) {      
      return;
    }

    const items = this.state.items;

    const movedItem = items.splice(source.index, 1);
    items.splice(destination.index, 0, movedItem[0]);

    this.setState({
       items
    });

    if(!this.state.sortByQuery && !this.state.query && !this.state.sort) {
      resetData(this.state.items);
    }
  };

  onFilterByDate = (e) => {
    e.preventDefault();
    const id = e.target.id;
    if (!id){
      this.setState({sortByQuery:''})
    }
    this.setState({sortByQuery: id})

  };

  sortingRules = (items) =>{
    const {checkedArr, itemsArr} = items.reduce((accumArr,item)=>{
      if (item.isChecked){
        accumArr.checkedArr.push(item)
      }else{
        accumArr.itemsArr.push(item);
      }
      return accumArr;
    },{checkedArr:[],itemsArr:[]});


    itemsArr.sort((a, b) => {
      return +b.priority - +a.priority;
    });
    return [...itemsArr, ...checkedArr];

  };

  filterRule = (array, query) => {

    let dateObj = new Date();

    dateObj.setHours(0,0,0,0);
    let timeStamp;
    switch (query) {
      case "tomorrow":
        timeStamp = dateObj.setDate(dateObj.getDate()+1);
        return array.filter(item =>{
              let itemDate = new Date(item.date);
              itemDate.setHours(0,0,0,0);
              itemDate = itemDate.getTime();

              return (itemDate - timeStamp) === 0
            }
        );
      case "dayAfter":
        timeStamp = dateObj.setDate(dateObj.getDate()+2);
        return array.filter(item =>{
              let itemDate = new Date(item.date);
              itemDate.setHours(0,0,0,0);
              itemDate = itemDate.getTime();

              return (itemDate - timeStamp) === 0
            }
        );
      case "thisWeek":
        timeStamp = dateObj.setDate(dateObj.getDate()+7);
        return array.filter(item =>{
              let itemDate = new Date(item.date);
              itemDate.setHours(0,0,0,0);
              itemDate = itemDate.getTime();
              return (timeStamp - itemDate) >= 0 && (timeStamp - itemDate) <=608400000
            }
        );
      default: return array;
    }
  };

  onTagDelete = (tagId, itemId = false) => {
    const items = this.state.items.map((item) => {
        item.selectedTags = item.selectedTags.filter((tag) => {
          return itemId ? (tag.id !== tagId || item.id !== itemId) : tag.id !== tagId;
        });
        if(!itemId) {
          item.tags = item.tags.filter((tag) => {
              return tag.id !== tagId;
          });
        }
        return item;
    });

    resetData(items);
    this.setState({
        items: items
    });
  };

  render() {
    const {
      items,
      loading,
      isError,
      isErrorDate,
      editBody,
      openModal,
      query,
      tagOpen,
      sortByQuery,
      sort,
      searchByTags,
    } = this.state;
    let updatedItemList;

    if (searchByTags) {
      updatedItemList = items.filter(item=>{
        const path = item.selectedTags;
        let str ='';

        for (let i = 0; i<path.length; i++){
          str += ' ' + path[i].tagName;
        }
        return str.toLowerCase().search(query.toLowerCase()) !== -1
      })
    } else {
      updatedItemList = items.filter(item => item.name.toLowerCase().search(query.toLowerCase()) !== -1);
    }

    updatedItemList = this.filterRule(updatedItemList, sortByQuery);

    if (sort){
      updatedItemList = this.sortingRules(updatedItemList)
    }
	
    return (
        <Fragment>
          <QueueAnim type={["top", "top"]}>
            <nav
                key="1"
                className="nav fixed-top  bg-adv border-bott shadow navbar navbar-expand-lg"
            >
              <SearchBar className="float-right" onSearch={this.searchingItem} />
              <button className="newButt newButt--search ml-2" onClick={()=>{this.setState(({searchByTags})=>{return{searchByTags: !searchByTags}})}} >
                {searchByTags ? 'Ищем по тегам' : 'Ищем по имени'}
              </button>
            </nav>
          </QueueAnim>
          <div className="container-fluid mt-5">
            <div className="row">
              <div
                  className={
                    "col-lg-2  bg-light side-panel " + ((openModal && "bg-dark" ) || (tagOpen && "bg-dark"))
                  }
              >
                <QueueAnim
                    component="div"
                    type="bottom"
                    delay={300}
                    className="list-group"
                >
                  <button
                      key="addBtn"
                      onClick={this.showModal}
                      className="btn newButt"
                      disabled={this.state.tagOpen}
                  >
                    Добавить комментарий
                  </button>
				  
                  <button
                      key="sortBtn"
                      onClick={this.sortItems}
                      className="btn newButt"
                  >
                    {!!sort ? "Отключить сортировку" : "Сортировать комментарии"}
                  </button>
				  
                  <button
                      key="tagsBtn"
                      onClick={this.showTag}
                      className="btn newButt"
                      disabled={this.state.openModal}
                  >
                    Добавить теги
                  </button>
				  
                  <div className="dropdown">                    
                    <button className="btn-tags newButt dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Фильтр по времени 
                    </button>
                    <span className="float-right w-100 text-dark">
                    
                      {sortByQuery === "tomorrow" ? "Filtering till tomorrow" :
                      sortByQuery === "dayAfter" ? "Filtering till the day after tomorow" :
                      sortByQuery === "thisWeek" ? "Filtering till next week" : ""}
                    
                    </span>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <li className="dropdown-item" id="" onClick={this.onFilterByDate}>без ограничений</li>
                      <li className="dropdown-item" id="tomorrow" onClick={this.onFilterByDate}>до завтра</li>
                      <li className="dropdown-item" id="dayAfter" onClick={this.onFilterByDate}>до послезавтра</li>
                      <li className="dropdown-item" id="thisWeek" onClick={this.onFilterByDate}>в течении недели</li>
                    </div>
                  </div>
                </QueueAnim>
              </div>
              <div className="col-lg-10 overflow-scroll pt-3">
                <QueueAnim
                    type={["left", "left"]}
                    duration={700}
                    animConfig={[
                      { translateX: [0, -400] },
                      { translateX: [0, -400] }
                    ]}
                >
                  {openModal && !tagOpen && (
                      <ModalWindow
                          key="aa"
                          onExit={this.showModal}
                          onEditItem={this.confirmEdit}
                          editionForm={editBody}
                          errorIndicatorName={isError}
                          errorIndicatorDate={isErrorDate}
                          onAdd={this.addItem}
                          sortItems={this.sortItems}
                      />
                  )}
                  {!openModal && tagOpen && <TagModal key="bbb" onTagDelete={this.onTagDelete} onExit={this.showTag} />}
                </QueueAnim>
                <DragDropContext
                    onDragEnd={this.onDragEnd}
                >
                  <ItemList
                      onDeleted={this.deleteItem}
                      onTicked={this.tickItem}
                      onEdited={this.editItem}
                      items={updatedItemList}
                      loading={loading}
                      sortByQuery={sortByQuery}
                      query={query}
                      sort={sort}
                      onTagDelete={this.onTagDelete}
                  />
                </DragDropContext>
              </div>
            </div>
          </div>
        </Fragment>
    );
  }
}
